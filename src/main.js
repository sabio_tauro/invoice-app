import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import VueAxios from 'vue-axios'

import App from './App.vue'
import Home from './components/Home/Home.vue'
import Products from './components/Products/Products.vue'
import Customers from './components/Customers/Customers.vue'
import SingleInvoice from './components/Invoices/SingleInvoice.vue'

Vue.use(VueRouter);
Vue.use(VueAxios, axios);

new Vue({
  el: '#app',
  render: h => h(App),
  router: new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'current-link',
    routes: [
      {
        path: '/',
        component: Home
      },
      {
        path: '/invoices/:id',
        component: SingleInvoice
      },
      {
        path: '/products',
        component: Products
      },
      {
        path: '/customers',
        component: Customers
      }
    ]
  })
})
